package base;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;

import java.io.IOException;
import java.util.Properties;

import static base.readPropertiesFromFile.readPropertiesFile;

public class BaseTest {

    private static String KEY;
    private static String TOKEN;

    protected static final String BASE_URL = "https://api.trello.com/1/";
    protected final String URL_BOARDS = BASE_URL + "boards/";
    protected final String URL_LISTS = BASE_URL + "lists/";
    protected final String URL_CARDS = BASE_URL + "cards/";
    protected static final String URL_ORGANIZATIONS = BASE_URL + "organizations/";

    protected static RequestSpecBuilder reqBuilder;
    protected static RequestSpecification reqSpec;

    @BeforeAll
    public static void beforeAll() throws IOException {

        Properties prop = readPropertiesFile("authentication.properties");
        KEY = prop.getProperty("key");
        TOKEN = prop.getProperty("token");

        reqBuilder = new RequestSpecBuilder();
        reqBuilder.addQueryParam("key", KEY);
        reqBuilder.addQueryParam("token", TOKEN);
        reqBuilder.setContentType(ContentType.JSON);

        reqSpec = reqBuilder.build();
    }

}
