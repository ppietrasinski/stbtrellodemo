package organizationTest;

import base.BaseTest;
import io.restassured.response.Response;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class OrgnaizationHelpMethods extends BaseTest {

    public static Response createNewOrganizationWithParams(Map<String, String> params) {

        return given()
                .spec(reqSpec)
                .queryParams(params)
                .when()
                .post(URL_ORGANIZATIONS)
                .then()
                .extract()
                .response();

    }

    public static void deleteOrganization(String organizationId) {

        given()
                .spec(reqSpec)
                .pathParam("id", organizationId)
                .when()
                .delete(URL_ORGANIZATIONS + "{id}");

    }



}
