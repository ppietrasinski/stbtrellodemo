package organizationTest;

import base.BaseTest;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static java.lang.Thread.sleep;
import static org.assertj.core.api.Assertions.*;

public class PostOrganizationTests extends BaseTest {

    /*
    Test of creating new TEAMS
     */

    private static String organizationId;
    private static String secondOrganizationId;

    public Map<String, String> params = new HashMap<>();
    public String displayName = "Test organization";
    public String name;
    public String website;

    @Test
    public void createNewOrganization() {

        displayName = "new organization without name and website";
        params.put("displayName", displayName);

        Response response = OrgnaizationHelpMethods.createNewOrganizationWithParams(params);
        JsonPath json = response.jsonPath();
        organizationId = json.getString("id");

        assertThat(json.getString("displayName")).isEqualTo(displayName);

    }

    @Test
    public void createNewOrganizationWithNameLengthLessThen3Signs() {

        name = "ab";
        displayName = "org with name length less then 3 signs";
        params.put("displayName", displayName);
        params.put("name", name);

        Response response = OrgnaizationHelpMethods.createNewOrganizationWithParams(params);
        JsonPath json = response.jsonPath();

        organizationId = json.getString("id");

        assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_OK);
        assertThat(json.getString("name").startsWith(name));

    }

    @Test
    public void createNewOrganizationWithNameCointainingUnallowedSigns() {

        name = "#%#$%$#    RE";
        displayName = "org with name containing unallowed signs";
        params.put("displayName", displayName);
        params.put("name", name);
        String regex = "[a-z\\d_]+";

        Response response = OrgnaizationHelpMethods.createNewOrganizationWithParams(params);
        JsonPath json = response.jsonPath();

        organizationId = json.getString("id");

        assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_OK);
        assertThat(json.getString("name")).matches(regex);

    }

    @Test
    public void createNewOrganizationWithNonUniqueName() {

        name = "bncsh_test_organisation_test_34567";
        displayName = "org with duplicate name";
        params.put("displayName", displayName);
        params.put("name", name);

        Response firstResponse = OrgnaizationHelpMethods.createNewOrganizationWithParams(params);
        Response secondResponse = OrgnaizationHelpMethods.createNewOrganizationWithParams(params);
        JsonPath jsonOne = firstResponse.jsonPath();
        JsonPath jsonTwo = secondResponse.jsonPath();

        assertThat(firstResponse.statusCode()).isEqualTo(HttpStatus.SC_OK);
        assertThat(secondResponse.statusCode()).isEqualTo(HttpStatus.SC_OK);
        assertThat(jsonOne.getString("name")).isNotEqualTo(jsonTwo.getString("name"));

        organizationId = jsonOne.getString("id");
        secondOrganizationId = jsonTwo.getString("id");

    }

    @Test
    public void createNewOrganizationWithWebsiteWithHttpStart() {

        website = "http://test.gov.com";
        displayName = "org with www starts with http";
        params.put("displayName", displayName);
        params.put("website", website);

        Response response = OrgnaizationHelpMethods.createNewOrganizationWithParams(params);
        JsonPath json= response.jsonPath();

        Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_OK);
        Assertions.assertThat(json.getString("website")).startsWith("http://");

        organizationId = json.getString("id");

    }

    @Test
    public void createNewOrganizationWithWebsiteWithHttpsStart() {

        website = "https://test.gov.com";
        displayName = "org with www starts with https";
        params.put("displayName", displayName);
        params.put("website", website);

        Response response = OrgnaizationHelpMethods.createNewOrganizationWithParams(params);
        JsonPath json= response.jsonPath();

        Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_OK);
        Assertions.assertThat(json.getString("website")).startsWith("https://");

        organizationId = json.getString("id");

    }

    @Test
    public void createNewOrganizationWithWebsiteOtherStartThenHttp() {

        website = "gkijnkjk";
        displayName = "org with www starts without http";
        params.put("displayName", displayName);
        params.put("website", website);

        Response response = OrgnaizationHelpMethods.createNewOrganizationWithParams(params);

        JsonPath json= response.jsonPath();

        Assertions.assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_OK);
        Assertions.assertThat(json.getString("website")).startsWith("http://");

        organizationId = json.getString("id");

        System.out.println(response.asString());
    }

    @AfterEach
    public void afterEachDeleteOrganization() {

        OrgnaizationHelpMethods.deleteOrganization(organizationId);

        if (secondOrganizationId != null) {
            OrgnaizationHelpMethods.deleteOrganization(secondOrganizationId);
            secondOrganizationId = null;
        }

        name = null;
        website = null;
        params.clear();

    }

}
