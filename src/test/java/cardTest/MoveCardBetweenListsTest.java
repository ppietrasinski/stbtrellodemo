package cardTest;

import base.BaseTest;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MoveCardBetweenListsTest extends BaseTest {

    private static String boardId;
    private static String list1Id;
    private static String list2Id;
    private static String cardId;

    private JsonPath createNewListInBoard(String boardId, String listName) {

        Response response = given()
                .spec(reqSpec)
                .queryParam("name", listName)
                .queryParam("idBoard", boardId)
                .when()
                .post(URL_LISTS)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        return response.jsonPath();

    }

    @Test
    @Order(1)
    public void createNewBoard() {

        String boardName = "My e2e board";

        Response response = given()
                .spec(reqSpec)
                .queryParam("name", boardName)
                .queryParam("defaultLists", false)
                .when()
                .post(URL_BOARDS)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("name")).isEqualTo(boardName);

        boardId = json.getString("id");

    }

    @Test
    @Order(2)
    public void createNewLists() {

        String firstListName = "list 1";
        String secondListName = "list 2";

        JsonPath jsonlist1 = createNewListInBoard(boardId, firstListName);
        JsonPath jsonlist2 = createNewListInBoard(boardId, secondListName);

        list1Id = jsonlist1.getString("id");
        list2Id = jsonlist2.getString("id");

        assertThat(jsonlist1.getString("name")).isEqualTo(firstListName);
        assertThat(jsonlist2.getString("name")).isEqualTo(secondListName);

        Response responseBoardGet = given()
                .spec(reqSpec)
                .pathParam("boardId", boardId)
                .when()
                .get(URL_BOARDS + "{boardId}/lists" )
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath jsonBoardGet = responseBoardGet.jsonPath();
        List<String> lists = jsonBoardGet.getList("id");

        assertThat(lists).hasSize(2);

    }

    @Test
    @Order(3)
    public void addCardInFirstList() {

        String cardName = "mu e2e card";

        Response response = given()
                .spec(reqSpec)
                .queryParam("idList", list1Id)
                .queryParam("name", cardName)
                .when()
                .post(URL_CARDS)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        cardId = json.getString("id");

        assertThat(json.getString("idBoard")).isEqualTo(boardId);
        assertThat(json.getString("name")).isEqualTo(cardName);

//        System.out.println("card id: "+cardId);
//        System.out.println("list 1 id:"+list1Id);
//        System.out.println("list 2 id:"+list2Id);

    }

    @Test
    @Order(4)
    public void moveCardToSecondList() {

        Response response = given()
                .spec(reqSpec)
                .pathParam("id", cardId)
                .queryParam("idList", list2Id)
                .when()
                .put(URL_CARDS + "{id}")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        assertThat(json.getString("idList")).isEqualTo(list2Id);

    }

    @Test
    @Order(5)
    public void deleteBoard() {

        Response response = given()
                .spec(reqSpec)
                .pathParam("id", boardId)
                .delete(URL_BOARDS + "{id}")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

        assertThat(json.getString("_value")).isEqualTo(null);

    }

}
