package boardTest;

import base.BaseTest;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import java.util.List;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.*;

public class BoardTest extends BaseTest {

    public void deleteBoard(String boardId) {
        given()
                .spec(reqSpec)
                .pathParam("id", boardId)
                .delete(URL_BOARDS + "{id}")
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void createNewBoard() {

        String boardName = "My first board";

        Response response = given()
                .spec(reqSpec)
                .queryParam("name", boardName)
                .when()
                .post(URL_BOARDS)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath json = response.jsonPath();
        assertThat(json.getString("name")).isEqualTo(boardName);

        String boardId = json.getString("id");
        deleteBoard(boardId);

    }

    @Test
    public void createNewBoardWithEmptyName() {

        String boardName = "";

        Response response = given()
                .spec(reqSpec)
                .queryParam("name", boardName)
                .when()
                .post(URL_BOARDS)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .extract()
                .response();

        JsonPath json = response.jsonPath();

    }

    @Test
    public void createNewBoardWithoutDefaultLists() {

        String boardName = "Board without default lists";

        Response responsePost = given()
                .spec(reqSpec)
                .queryParam("name", boardName)
                .queryParam("defaultLists", false)
                .when()
                .post(URL_BOARDS)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath jsonPost = responsePost.jsonPath();

        assertThat(jsonPost.getString("name")).isEqualTo(boardName);

        String boardId = jsonPost.getString("id");

        Response responseGet = given()
                .spec(reqSpec)
                .pathParam("id", boardId)
                .when()
                .get(URL_BOARDS + "{id}/lists")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath jsonGet = responseGet.jsonPath();
        List<String> idList = jsonGet.getList("id");

        assertThat(idList.size()).isEqualTo(0);

        deleteBoard(boardId);

    }

    @Test
    public void createNewBoardWithDefaultLists() {

        String boardName = "Board without default lists";

        Response responsePost = given()
                .spec(reqSpec)
                .queryParam("name", boardName)
                .when()
                .post(URL_BOARDS)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath jsonPost = responsePost.jsonPath();
        assertThat(jsonPost.getString("name")).isEqualTo(boardName);

        String boardId = jsonPost.getString("id");

        Response responseGet = given()
                .spec(reqSpec)
                .pathParam("id", boardId)
                .when()
                .get(URL_BOARDS + "{id}/lists")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .response();

        JsonPath jsonGet = responseGet.jsonPath();
        List<String> names = jsonGet.getList("name");

        assertThat(names)
                .hasSize(3)
                .contains("To Do", "Doing","Done");

        deleteBoard(boardId);

    }

}
